﻿using System.Windows;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System;

namespace Bookit
{
    public partial class App : Application
    {

        /// <summary>
        /// Provides easy access to the root frame of the Phone Application.
        /// </summary>
        /// <returns>The root frame of the Phone Application.</returns>
        public PhoneApplicationFrame RootFrame { get; private set; }

        /// <summary>
        /// Constructor for the Application object.
        /// </summary>
        public App()
        {
            // Global handler for uncaught exceptions. 
            UnhandledException += Application_UnhandledException;

            // Standard Silverlight initialization
            InitializeComponent();

            // Phone-specific initialization
            InitializePhoneApplication();

            // Show graphics profiling information while debugging.
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // Display the current frame rate counters.
                Application.Current.Host.Settings.EnableFrameRateCounter = true;

                // Show the areas of the app that are being redrawn in each frame.
                //Application.Current.Host.Settings.EnableRedrawRegions = true;

                // Enable non-production analysis visualization mode, 
                // which shows areas of a page that are handed off to GPU with a colored overlay.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;

                // Disable the application idle detection by setting the UserIdleDetectionMode property of the
                // application's PhoneApplicationService object to Disabled.
                // Caution:- Use this under debug mode only. Application that disables user idle detection will continue to run
                // and consume battery power when the user is not using the phone.
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }

        }

        // Code to execute when the application is launching (eg, from Start)
        // This code will not execute when the application is reactivated
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {
            IsNeedInitApp = true;
            InitApp();
            UmengSDK.UmengAnalytics.onLaunching("4f1d5445527015681a000004");
            UmengSDK.UmengAnalytics.setDebug(false);
            GeneralClass.CheckUpdate(GeneralClass.AppUpdateMode.Silent);         //检查更新
        }

        // Code to execute when the application is activated (brought to foreground)
        // This code will not execute when the application is first launched
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
            IsNeedInitApp = true;
            InitApp();
        }

        // Code to execute when the application is deactivated (sent to background)
        // This code will not execute when the application is closing
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
            // Ensure that required application state is persisted here.
            SaveStatus();
        }

        // Code to execute when the application is closing (eg, user hit Back)
        // This code will not execute when the application is deactivated
        private void Application_Closing(object sender, ClosingEventArgs e)
        {
            SaveStatus();
        }

        // Code to execute if a navigation fails
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // A navigation has failed; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        // Code to execute on Unhandled Exceptions
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            MessageBox.Show("发生意外错误!" + Environment.NewLine + e.ExceptionObject.Message);
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        public static string ReturnedBarcode { get; set; }
        public static Settings CurrentSetting { get; set; }
        public static bool IsNeedInitApp { get; set; }
        public static bool IsNeedLoadSearchList { get; set; }
        public static BorrowList BorrowListInstance { get; set; }
        public static ReturnReminderList ReturnReminderListInstance { get; set; }
        public static SearchResultList SearchResultListInstance { get; set; }
        public static IsolatedStorageSettings storageSettings;

        /// <summary>
        /// 初始化应用,在Launching或Activated时运行
        /// </summary>
        private void InitApp()
        {
            if (!IsNeedInitApp)            //防止重复载入数据
                return;

            storageSettings  = IsolatedStorageSettings.ApplicationSettings;
            BorrowListInstance = new BorrowList();
            ReturnedBarcode = string.Empty;
            CurrentSetting = Settings.Load();
            SearchResultListInstance = SearchResultList.LoadFromCache();

            BorrowListInstance = BorrowList.LoadBorrowListFromLocal();

            ReturnReminderListInstance = ReturnReminderList.LoadReturnReminderListFromLocal();

            //TiltEffect.TiltableItems.Add(typeof(ButtonBase));
            //TiltEffect.TiltableItems.Add(typeof(LongListSelector));

            IsNeedInitApp = false;
        }

        private void SaveStatus()
        {
            CurrentSetting.Save();          //保存设置
            BorrowListInstance.SaveList();
            SearchResultListInstance.SaveCache();
            ReturnReminderListInstance.SaveList();
        }

        #region Phone application initialization

        // Avoid double-initialization
        private bool phoneApplicationInitialized = false;

        // Do not add any additional code to this method
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;

            // Create the frame but don't set it as RootVisual yet; this allows the splash
            // screen to remain active until the application is ready to render.
            RootFrame = new TransitionFrame(); // PhoneApplicationFrame(); 
            RootFrame.Navigated += CompleteInitializePhoneApplication;

            // Handle navigation failures
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;

            // Ensure we don't initialize again
            phoneApplicationInitialized = true;
        }

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Set the root visual to allow the application to render
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        #endregion
    }
}