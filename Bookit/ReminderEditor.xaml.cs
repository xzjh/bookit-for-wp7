﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using System.Windows.Resources;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Phone.Scheduler;

namespace Bookit
{
    public partial class ReminderEditor : PhoneApplicationPage
    {
        public ReminderEditor()
        {
            InitializeComponent();
        }

        //-1新建
        private int reminderIndex = -1;
        //针对index>=0时, 若usermode==1, 则不设置日期,内容
        private int userMode = 0;

        private void mnuOK_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (txtReminderContent.Text.Trim().Equals(string.Empty))
                {
                    MessageBox.Show("提醒内容不得为空!");
                    return;
                }
                DateTime newTime=dpStartDate.Value.Value.Date.Add(tpStartTime.Value.Value.TimeOfDay);
                if (newTime <= DateTime.Now)
                {
                    MessageBox.Show("提醒时间不得早于当前时间!");
                    return;
                }
                if (reminderIndex == -1)       //新增
                {
                    ReturnReminder newReturnReminder = new ReturnReminder(
                        "BOOKIT_REMINDER_" + GeneralClass.GetUnixTime(),
                        txtReminderContent.Text, newTime);
                    App.ReturnReminderListInstance.TheList.Add(newReturnReminder);

                    Reminder newReminder = new Reminder(newReturnReminder.Name);
                    newReminder.Content = newReturnReminder.Content;
                    newReminder.BeginTime = newReturnReminder.StartTime;
                    newReminder.ExpirationTime = newReturnReminder.StartTime.AddSeconds(10);
                    newReminder.RecurrenceType = RecurrenceInterval.None;
                    ScheduledActionService.Add(newReminder);

                    MessageBox.Show(newReturnReminder.Content + Environment.NewLine +
                        Environment.NewLine +
                        "提醒已增加!");
                }
                else  //修改
                {
                    if (ScheduledActionService.Find(App.ReturnReminderListInstance.TheList[reminderIndex].Name) != null)
                        ScheduledActionService.Remove(App.ReturnReminderListInstance.TheList[reminderIndex].Name);

                    ReturnReminder thisReminder = App.ReturnReminderListInstance.TheList[reminderIndex];
                    //更新信息
                    thisReminder.Content = txtReminderContent.Text;
                    thisReminder.StartTime = newTime;

                    Reminder newReminder = new Reminder(thisReminder.Name);
                    newReminder.Content = thisReminder.Content;
                    newReminder.BeginTime = thisReminder.StartTime;
                    newReminder.ExpirationTime = thisReminder.StartTime.AddSeconds(10);
                    newReminder.RecurrenceType = RecurrenceInterval.None;
                    ScheduledActionService.Add(newReminder);

                    MessageBox.Show(thisReminder.Content + Environment.NewLine +
                        Environment.NewLine +
                        "提醒已更新!");
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

            NavigationService.GoBack();
        }

        private void mnuCancel_Click(object sender, System.EventArgs e)
        {
            NavigationService.GoBack();
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            reminderIndex = Int32.Parse(NavigationContext.QueryString["index"]);
            userMode = Int32.Parse(NavigationContext.QueryString["usermode"]);
            if (userMode == 1)    //正在编辑, 程序不再改变内容
                return;
            if (reminderIndex == -1)    //新建reminder
            {
                if (NavigationContext.QueryString.ContainsKey("content"))
                    txtReminderContent.Text = NavigationContext.QueryString["content"];
            }
            else   //编辑现有reminder
            {
                dpStartDate.Value = App.ReturnReminderListInstance.TheList[reminderIndex].StartTime;
                tpStartTime.Value = App.ReturnReminderListInstance.TheList[reminderIndex].StartTime;
                txtReminderContent.Text = App.ReturnReminderListInstance.TheList[reminderIndex].Content;
                NavigationContext.QueryString["usermode"] = "1";      //用户编辑模式
            }
        }

        private void mnuRemove_Click(object sender, System.EventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("确定要删除此条提醒吗?", "删除提醒", MessageBoxButton.OKCancel);
            if (mbr == MessageBoxResult.Cancel)
                return;

            if (reminderIndex == -1)
            {
                NavigationService.GoBack();
            }
            else
            {
                App.ReturnReminderListInstance.RemoveReminder(reminderIndex);
                NavigationService.GoBack();
            }
        }
    }
}