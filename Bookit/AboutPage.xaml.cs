﻿using System;
using Microsoft.Phone.Controls;
using System.Windows.Data;
using Microsoft.Phone.Tasks;
using System.Windows;

namespace Bookit
{
    public partial class AboutPage : PhoneApplicationPage
    {
        public AboutPage()
        {
            InitializeComponent();
        }
        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            UmengSDK.UmengAnalytics.onEvent("about");
        }
        private void mnuContactUs_Click(object sender, System.EventArgs e)
        {
            EmailComposeTask emailComposer = new EmailComposeTask();
            emailComposer.To = "bookitapp@gmail.com";
            emailComposer.Show();
        }

        private void mnuShareWithFriends_Click(object sender, System.EventArgs e)
        {
            SmsComposeTask smsComposer = new SmsComposeTask();
            smsComposer.Body = "我向你推荐一款应用：Bookit。它可以查询大陆香港及台湾部分高校的高校图书馆馆藏，用户只需键入书的相关信息（关键词，如：标题、作者、ISBN号）或者直接扫描书的ISBN号便可获得相关图书在高校图书馆的馆藏信息。去电子市场下载试试吧！";
            smsComposer.Show();
        }

        private void mnuCheckUpdate_Click(object sender, System.EventArgs e)
        {
            GeneralClass.CheckUpdate(GeneralClass.AppUpdateMode.Normal);
        }
    }

    /// <summary>
    /// AppName=>"关于" + AppName
    /// </summary>
    public class AppNameToAboutTitle : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return "关于 " + value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}