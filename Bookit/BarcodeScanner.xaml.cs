﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using WP7_Barcode_Library;
using System.Windows.Navigation;

namespace Bookit
{
    public partial class BarcodeScanner : PhoneApplicationPage
    {
        bool isSuccess;

        public BarcodeScanner()
        {
            InitializeComponent();
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            UmengSDK.UmengAnalytics.onEvent("scanbarcode");
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Detect if page is navigating back from BarcodeSampleChooser and start processing
            try
            {
                string strFlag;
                if (NavigationContext.QueryString.TryGetValue("LOADSAMPLE", out strFlag))
                {
                    if (BarcodeSampleItemManager.SelectedItem != null)
                    {
                        WP7BarcodeManager.ScanBarcode(BarcodeSampleItemManager.SelectedItem.FileURI, new Action<BarcodeCaptureResult>(this.BarcodeResults_Finished));
                    }
                    else
                    {
                        MessageBox.Show("Error: Sample chooser canceled");
                    }
                    NavigationContext.QueryString.Remove("LOADSAMPLE");
                }
                else if (NavigationContext.QueryString.TryGetValue("action", out strFlag))
                {
                    if (strFlag.Equals("scanbarcode"))
                    {
                        NavigationContext.QueryString.Remove("action");
                        BeginScanBarcode();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                txbResult.Text = "识别失败";
                isSuccess = false;
            }
        }

        public void BarcodeResults_Finished(BarcodeCaptureResult BCResults)
        {
            try
            {
                if (WP7BarcodeManager.LastCaptureResults.BarcodeImage != null)
                {
                    imgCapture.Source = WP7BarcodeManager.LastCaptureResults.BarcodeImage; //Display image
                }
                else
                {
                    //No image captured
                }
                if (BCResults.State == CaptureState.Success)
                {
                    txbResult.Text = BCResults.BarcodeText; //Use results
                    isSuccess = true;
                }
                else
                {
                    MessageBox.Show(BCResults.ErrorMessage);
                    txbResult.Text = "识别失败";
                    isSuccess = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("条码识别失败: {0}", ex.Message));
                txbResult.Text = "识别失败";
                isSuccess = false;
            }
        }

        //Extra code required only if you are bypassing the camera
        private void NavigationService_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back && NavigationService.CurrentSource == BarcodeSampleItemManager.BarcodeChooserURI)
            {
                NavigationContext.QueryString["LOADSAMPLE"] = "TRUE";//Set flag to load sample
            }
            else
            {
                NavigationContext.QueryString.Remove("LOADSAMPLE");
            }
        }

        private void BeginScanBarcode()
        {
            try
            {
                WP7BarcodeManager.ScanMode = com.google.zxing.BarcodeFormat.UPC_EAN;
                bool bBypassCamera = false;
                if (bBypassCamera == true) //Use Barcode Sample Chooser "Task" if bypass camera setting is active (Emulator/demo mode)
                {
                    NavigationService.Navigating += new NavigatingCancelEventHandler(NavigationService_Navigating);
                    NavigationService.Navigate(BarcodeSampleItemManager.BarcodeChooserURI); //Navigate to barcode chooser page located in WP7_Barcode_Library.WP7Code
                }
                else //Use BarcodeManager to start camera and scan image
                {
                    WP7BarcodeManager.ScanBarcode(BarcodeResults_Finished); //Provide callback method
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                txbResult.Text = "识别失败";
                isSuccess = false;
            }
        }

        private void mnuScanBarCode_Click(object sender, System.EventArgs e)
        {
            BeginScanBarcode();
        }

        private void mnuGoBack_Click(object sender, System.EventArgs e)
        {
            if (isSuccess)
                App.ReturnedBarcode = txbResult.Text;
            NavigationService.GoBack();
        }

        private void btnStartSearch_Click(object sender, RoutedEventArgs e)
        {
            if (!isSuccess)
            {
                MessageBox.Show("条码识别失败, 无法搜索!");
                return;
            }
            mnuGoBack_Click(sender, e);
        }
    }
}