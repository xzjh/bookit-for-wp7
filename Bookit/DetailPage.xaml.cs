﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Bookit
{
    public partial class DetailPage : PhoneApplicationPage
    {
        public DetailPage()
        {
            InitializeComponent();
        }
        private Book thisBook;
        private enum ListType { SearchList = 0, BorrowList = 1 };
        private ListType listType;          //列表类型
        private int listIndex;              //索引
        //private int delta = 1;
        private bool isDeletedFromBorrowList = false;
        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            UmengSDK.UmengAnalytics.onEvent("detailinfo");
            mnuPrevItem = (ApplicationBarIconButton)ApplicationBar.Buttons[0];
            mnuAddToBorrowList = (ApplicationBarIconButton)ApplicationBar.Buttons[1];
            mnuNextItem = (ApplicationBarIconButton)ApplicationBar.Buttons[3];
            string[] query = NavigationContext.QueryString["index"].Split('_');
            listIndex = Int32.Parse(query[1]);

            if (query[0].Equals("s"))           //搜索列表
            {
                listType = ListType.SearchList;
            }
            else                                //借阅列表
            {
                listType = ListType.BorrowList;
            }

            LoadDetailInfo();

        }
        /// <summary>
        /// 更新当前书目的馆藏和摘要
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuRefresh_Click(object sender, EventArgs e)
        {
            thisBook.getHoldingInfoFromNet();
            thisBook.getSummaryFromNet();
        }
        /// <summary>
        /// 添加当前数目到借阅列表/从借阅列表移除当前数目
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuAddToBorrowList_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < App.BorrowListInstance.TheList.Count; i++)
            {
                if (thisBook.Isbn.Equals(App.BorrowListInstance.TheList[i].Isbn))
                {
                    if (MessageBox.Show("本书将从借阅列表里移除，是否继续？", "移除本书", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                    {
                        App.BorrowListInstance.TheList.RemoveAt(i);
                        App.BorrowListInstance.CheckIfEmpty();
                        MessageBox.Show("该书已从借阅列表中移除");
                        isDeletedFromBorrowList = true;
                    }
                    return;
                }
            }
            if (thisBook.Isbn == Bookit.Resources.AppStrings.ISBNNone)
            {
                MessageBox.Show("该书没有ISBN信息，无法添加到借阅列表！");
                mnuAddToBorrowList.IsEnabled = false;
            }
            else
            {
                App.BorrowListInstance.Add(thisBook);
                MessageBox.Show("已将该书加入借阅列表中");
            }
        }
        /// <summary>
        /// 载入当前书目信息
        /// </summary>
        /// <param name="isSuccess"></param>
        private void LoadDetailInfo (int isSuccess = 1)
        {
            if (isSuccess > 0)
            {
                if (listType == ListType.SearchList)
                {
                    //搜索列表
                    App.SearchResultListInstance.SearchResultArgsIns.ListIndex = listIndex;
                    thisBook = App.SearchResultListInstance.TheList[listIndex];
                    if (thisBook.Summary != Bookit.Resources.AppStrings.SummaryLoading && thisBook.Summary != Bookit.Resources.AppStrings.SummaryNone)
                        thisBook.getSummaryFromNet();
                }
                else
                {
                    //更新当前索引
                    App.BorrowListInstance.currentIndex = listIndex;
                    thisBook = App.BorrowListInstance.TheList[listIndex];
                }
                UpdateUI();
            }
            else
            {
                //加载出错
                MessageBox.Show("加载异常"); 
                NavigationService.GoBack();
            }
        }

        //数组越界的问题在mnuPrevItem_Click和mnuNextItem_Click里处理
        /// <summary>
        /// 跳转到前一本书
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuPrevItem_Click(object sender, System.EventArgs e)
        {
            UmengSDK.UmengAnalytics.onEvent("detailinfo");
            listIndex--;
            if (listType == ListType.SearchList)
            {
                //SearchList
                if (listIndex == -1)
                {
                    //加载前一页并加载前一页的最后一本书
                    listIndex += App.SearchResultListInstance.SearchResultArgsIns.MaxResults;
                    App.SearchResultListInstance.SearchResultArgsIns.StartIndex = App.SearchResultListInstance.SearchResultArgsIns.StartIndex - App.SearchResultListInstance.SearchResultArgsIns.MaxResults >= 1 ? App.SearchResultListInstance.SearchResultArgsIns.StartIndex - App.SearchResultListInstance.SearchResultArgsIns.MaxResults : 1;
                    App.SearchResultListInstance.LoadSearchResultListFromNet(App.CurrentSetting.keyWord, LoadDetailInfo, App.SearchResultListInstance.SearchResultArgsIns.MaxResults, App.SearchResultListInstance.SearchResultArgsIns.StartIndex);
                }
                else
                {
                    //加载前一本书
                    LoadDetailInfo();
                }
            }
            else            //借阅列表
            {
                LoadDetailInfo();
                isDeletedFromBorrowList = false;
            }
        }
        /// <summary>
        /// 跳转到后一本书
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuNextItem_Click(object sender, System.EventArgs e)
        {
            UmengSDK.UmengAnalytics.onEvent("detailinfo");
            if (listType == ListType.SearchList)
            {
                listIndex++;
                if (listIndex == App.SearchResultListInstance.TheList.Count)
                {
                    //加载后一页的第一本书
                    listIndex = 0;
                    App.SearchResultListInstance.SearchResultArgsIns.StartIndex += App.SearchResultListInstance.SearchResultArgsIns.MaxResults;
                    App.SearchResultListInstance.LoadSearchResultListFromNet(App.CurrentSetting.keyWord, LoadDetailInfo, App.SearchResultListInstance.SearchResultArgsIns.MaxResults, App.SearchResultListInstance.SearchResultArgsIns.StartIndex);
                }
                else
                {
                    //加载当页下一本书
                    LoadDetailInfo();
                }
            }
            else            //借阅列表
            {
                if (!isDeletedFromBorrowList)       //若没被删除,则listIndex加1,否则新下标不变
                    listIndex++;
                LoadDetailInfo();
                isDeletedFromBorrowList = false;
            }
        }
        /// <summary>
        /// 更新界面
        /// </summary>
        private void UpdateUI()
        {
            LayoutRoot.DataContext = thisBook;
            scrollViewer.ScrollToVerticalOffset(0);
            //判断是否有ISBN信息
            if (thisBook.Isbn == Bookit.Resources.AppStrings.ISBNNone)
            {
                mnuAddToBorrowList.IsEnabled = false;
            }
            else
            {
                mnuAddToBorrowList.IsEnabled = true;
            }
            //判断左右键
            if (listType == ListType.BorrowList)
            {
                //判断是否是第一个结果
                if (listIndex < App.BorrowListInstance.TheList.Count - 1)
                {
                    mnuNextItem.IsEnabled = true;
                }
                else
                {
                    mnuNextItem.IsEnabled = false;
                }
                //判断是否是最后一个结果
                if (listIndex > 0)
                {
                    mnuPrevItem.IsEnabled = true;
                }
                else
                {
                    mnuPrevItem.IsEnabled = false;
                }
            }
            else
            {
                //判断是否是第一个结果
                if (listIndex == 0 && App.SearchResultListInstance.SearchResultArgsIns.StartIndex == 1)
                {
                    mnuPrevItem.IsEnabled = false;
                }
                else
                {
                    mnuPrevItem.IsEnabled = true;
                }
                //判断是否是最后一个结果
                if (listIndex == App.SearchResultListInstance.TheList.Count - 1   //该页最后一项
                    && App.SearchResultListInstance.SearchResultArgsIns.StartIndex / App.SearchResultListInstance.SearchResultArgsIns.MaxResults + 1
                    == (App.SearchResultListInstance.SearchResultArgsIns.TotalResults + App.SearchResultListInstance.SearchResultArgsIns.MaxResults - 1)
                    / App.SearchResultListInstance.SearchResultArgsIns.MaxResults   //最后一页
                    )
                {
                    mnuNextItem.IsEnabled = false;
                }
                else
                {
                    mnuNextItem.IsEnabled = true;
                }
            }
        }

        private void mnuAddToReminderList_Click(object sender, System.EventArgs e)
        {
            NavigationService.Navigate(new Uri("/ReminderEditor.xaml?index=-1&usermode=0&content=归还 " + thisBook.Title, UriKind.Relative));
        }
    }
}