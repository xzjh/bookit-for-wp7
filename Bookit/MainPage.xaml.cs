﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using System.Windows.Data;
using Bookit.Resources;
using System.Net;
using System.Windows.Input;
using Newtonsoft.Json.Linq;

namespace Bookit
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            //扫描条码返回
            if (!App.ReturnedBarcode.Equals(string.Empty))
            {
                txtSearchKeyword.Text = App.ReturnedBarcode;
                App.ReturnedBarcode = string.Empty;
                btnSearch_Click(sender, e);
            }
            else if(!App.CurrentSetting.keyWord.Equals(string.Empty))
            {
                txtSearchKeyword.Text = App.CurrentSetting.keyWord;
            } 

            //设置数据绑定
            lbBorrowList.DataContext = App.BorrowListInstance.TheList;
            txbLibrary.DataContext = App.CurrentSetting;
            lbBorrowList.ScrollIntoView(lbBorrowList.Items[App.BorrowListInstance.currentIndex]);
            lbReminderList.DataContext = App.ReturnReminderListInstance.TheList;

            InputScope keyboard = new InputScope();         //更改键盘类型
            InputScopeName scopeName = new InputScopeName();
            scopeName.NameValue = InputScopeNameValue.Search;
            keyboard.Names.Add(scopeName);
            txtSearchKeyword.InputScope = keyboard;
        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/LibrarySelector.xaml", UriKind.RelativeOrAbsolute));
        }

        private void txtSearchKeyword_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtSearchKeyword.Text.Equals(AppStrings.NoKeyword))
            {
                txtSearchKeyword.Text = string.Empty;
            }
        }

        private void txtSearchKeyword_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtSearchKeyword.Text.Equals(string.Empty))
                txtSearchKeyword.Text = AppStrings.NoKeyword;
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (txtSearchKeyword.Text.Equals(AppStrings.NoKeyword) || txtSearchKeyword.Text.Equals(String.Empty))
            {
                MessageBox.Show("请输入关键字");
            }
            else
            {
                App.IsNeedLoadSearchList = true;
                NavigationService.Navigate(new Uri("/SearchList.xaml?keyword=" + HttpUtility.UrlEncode(txtSearchKeyword.Text), UriKind.RelativeOrAbsolute));
            }
        }

        private void btnScanBarcode_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/BarcodeScanner.xaml?action=scanbarcode", UriKind.RelativeOrAbsolute));
        }

        private void btnAbout_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.RelativeOrAbsolute));
        }

        private void lbBorrowList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Book thisBook = lbBorrowList.SelectedItem as Book;
            if (thisBook != null && thisBook.ImageUri == Bookit.Resources.AppStrings.EmptyImage)
            {
                MessageBox.Show("借阅列表为空，您可以在图书页面下方点“借阅列表”按钮添加！");
            }
            //borrowlist index=b_id
            else if (e.AddedItems.Count == 1)
            {
                NavigationService.Navigate(new Uri("/DetailPage.xaml?index=b_" + lbBorrowList.SelectedIndex.ToString(), UriKind.Relative));
            }
            lbBorrowList.SelectedIndex = -1;            //取消选中
        }

        private void txtSearchKeyword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                btnSearch_Click(sender, e);
        }

        private void btnNewReminder_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/ReminderEditor.xaml?index=-1&usermode=0", UriKind.Relative));
        }

        private void btnRemoveExpiredReminder_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("此操作将删除所有过期提醒, 确定继续吗?", "删除过期提醒", MessageBoxButton.OKCancel);
            if (mbr == MessageBoxResult.Cancel)
                return;

            for (int i = App.ReturnReminderListInstance.TheList.Count - 1; i >= 0; i--)
            {
                if (App.ReturnReminderListInstance.TheList[i].StartTime < DateTime.Now)
                    App.ReturnReminderListInstance.RemoveReminder(i);
            }
        }

        private void lbReminderList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ReturnReminder reminder = lbReminderList.SelectedItem as ReturnReminder;
            NavigationService.Navigate(new Uri("/ReminderEditor.xaml?usermode=0&index=" + lbReminderList.SelectedIndex.ToString(), UriKind.Relative));
            lbReminderList.SelectedIndex = -1;            //取消选中
        }
    }

    
    /// <summary>
    /// HoldingInfo to RequestNo
    /// </summary>
    public class HoldingInfoToRequestNoConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                string str = value.ToString();
                if (str == String.Empty)     //没有项
                {
                    return String.Empty;
                }
                int nl = str.IndexOf('\n');
                if (nl >= 0)
                    return str.Substring(0, nl);
                else
                    return str;
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}