﻿using System.Windows;
using Microsoft.Phone.Controls;
using System.Windows.Data;
using System;
using Microsoft.Phone.Shell;
using System.Net;

namespace Bookit
{
    public partial class SearchList : PhoneApplicationPage
    {
        public SearchList()
        {
            InitializeComponent();
        }

        public SearchResultList searchList;
        private ApplicationBarIconButton menuPrevPage, menuNextPage;
        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            UmengSDK.UmengAnalytics.onEvent("searchbooks");
            searchList = App.SearchResultListInstance;
            lstSearchList.DataContext = searchList.TheList;
            menuPrevPage = (ApplicationBarIconButton)ApplicationBar.Buttons[0];
            menuNextPage = (ApplicationBarIconButton)ApplicationBar.Buttons[2];

            if (App.IsNeedLoadSearchList)
            {
                App.CurrentSetting.keyWord = NavigationContext.QueryString["keyword"];
                App.SearchResultListInstance.SearchResultArgsIns.StartIndex = 1;                  //从第一页开始载入
                LoadSearchList();
                menuPrevPage.IsEnabled = false;
            }
            else
            {
                UpdateUI();
            }
        }

        private void LoadSearchList()                            //载入列表
        {
            App.SearchResultListInstance.SearchResultArgsIns.ListIndex = 0;     //从第一项开始
            progressBar.Visibility = System.Windows.Visibility.Visible;
            searchList.LoadSearchResultListFromNet(App.CurrentSetting.keyWord, DoAfterLoadingList, App.SearchResultListInstance.SearchResultArgsIns.MaxResults, App.SearchResultListInstance.SearchResultArgsIns.StartIndex);
        }

        public void DoAfterLoadingList(int isSuccess)
        {
            App.IsNeedLoadSearchList = false;
            progressBar.Visibility = System.Windows.Visibility.Collapsed;
            if (isSuccess > 0)      //成功
            {
                UpdateUI();

                if (lstSearchList.Items.Count >= 1)
                {
                    if (searchList.TheList.Count == 1 && App.SearchResultListInstance.SearchResultArgsIns.StartIndex == 1)       //若只有一个结果则直接载入
                        NavigationService.Navigate(new Uri("/DetailPage.xaml?index=s_0", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show("无搜索结果!");
                    if (App.SearchResultListInstance.SearchResultArgsIns.StartIndex == 1)            //是第一页
                        NavigationService.GoBack();
                }
            }
            else if (isSuccess == -1)               //失败
            {
                MessageBox.Show("图书信息获取失败，请检查网络设置并重新搜索");
                if (App.SearchResultListInstance.SearchResultArgsIns.StartIndex == 1)
                    NavigationService.GoBack();
            }
            else if (isSuccess == 0)
            {
                MessageBox.Show("矮油，没有找到你想要的书哎！要不，换个关键词吧！");
                NavigationService.GoBack();
            }
        }

        private void mnuPrevPage_Click(object sender, System.EventArgs e)
        {
            UmengSDK.UmengAnalytics.onEvent("searchbooks");
            App.IsNeedLoadSearchList = true;
            App.SearchResultListInstance.SearchResultArgsIns.StartIndex = App.SearchResultListInstance.SearchResultArgsIns.StartIndex - App.SearchResultListInstance.SearchResultArgsIns.MaxResults >= 1 ? App.SearchResultListInstance.SearchResultArgsIns.StartIndex - App.SearchResultListInstance.SearchResultArgsIns.MaxResults : 1;
            LoadSearchList();
        }

        private void mnuRefresh_Click(object sender, System.EventArgs e)
        {
            App.IsNeedLoadSearchList = true;
            LoadSearchList();
        }

        private void mnuNextPage_Click(object sender, System.EventArgs e)
        {
            UmengSDK.UmengAnalytics.onEvent("searchbooks");
            App.IsNeedLoadSearchList = true;
            App.SearchResultListInstance.SearchResultArgsIns.StartIndex += App.SearchResultListInstance.SearchResultArgsIns.MaxResults;
            LoadSearchList();
        }

        private void lstSearchList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //borrowlist index=s_id
            if (e.AddedItems.Count == 1)
            {
                App.SearchResultListInstance.SearchResultArgsIns.ListIndex = lstSearchList.SelectedIndex;
                NavigationService.Navigate(new Uri("/DetailPage.xaml?index=s_" + lstSearchList.SelectedIndex.ToString(), UriKind.Relative));
            }
            lstSearchList.SelectedIndex = -1;            //取消选中

        }

        private void UpdateUI()
        {
            //TODO: 在此更新txbSearchResultInfo
            txbSearchResultInfo.Text = "共"
                + App.SearchResultListInstance.SearchResultArgsIns.TotalResults
                + "个结果 当前第"
                + (App.SearchResultListInstance.SearchResultArgsIns.StartIndex / App.SearchResultListInstance.SearchResultArgsIns.MaxResults + 1)
                + "页/共"
                + ((App.SearchResultListInstance.SearchResultArgsIns.TotalResults + App.SearchResultListInstance.SearchResultArgsIns.MaxResults - 1)
                / App.SearchResultListInstance.SearchResultArgsIns.MaxResults)
                + "页";
            //txbSearchResultInfo.Text = searchInfo;
            if (App.SearchResultListInstance.SearchResultArgsIns.StartIndex <= 1)
                menuPrevPage.IsEnabled = false;                  //禁用"上一页"图标
            else
                menuPrevPage.IsEnabled = true;
            if (searchList.TheList.Count < App.SearchResultListInstance.SearchResultArgsIns.MaxResults)
                menuNextPage.IsEnabled = false;                  //禁用"下一页"图标
            else
                menuNextPage.IsEnabled = true;

            //调整滚动条位置
            lstSearchList.ScrollIntoView(lstSearchList.Items[App.SearchResultListInstance.SearchResultArgsIns.ListIndex]);
        }
    }
}