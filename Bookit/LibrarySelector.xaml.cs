﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using System.Windows.Resources;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows.Controls;

namespace Bookit
{
    public partial class LibrarySelector : PhoneApplicationPage
    {
        public LibrarySelector()
        {
            InitializeComponent();
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            List<LibraryItem> libraryList = new List<LibraryItem>();
            string[] separator = { Environment.NewLine };
            string[] libraries = Bookit.Resources.Files.LibraryList.Split(separator, StringSplitOptions.RemoveEmptyEntries);

            foreach (string strThisLibrary in libraries)
            {
                string[] libraryProperties = strThisLibrary.Split(',');
                LibraryItem thisLibrary = new LibraryItem()
                {
                    Category = libraryProperties[0],
                    Name = libraryProperties[1],
                    Id = libraryProperties[2],
                    Server = libraryProperties[3]
                };
                libraryList.Add(thisLibrary);
            }

            var librariesByCategory = from library in libraryList
                                      group library by library.Category into c
                                      orderby c.Key
                                      select new Group<LibraryItem>(c.Key, c);

            llsLibrary.ItemsSource = librariesByCategory;
            llsLibrary.SelectionChanged += LibrarySelectionChanged;             //绑定事件
        }


        private void LibrarySelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LibraryItem selectedLibrary = llsLibrary.SelectedItem as LibraryItem;
            if (selectedLibrary != null)
            {
                App.CurrentSetting.libKey = selectedLibrary.Id;
                App.CurrentSetting.LibName = selectedLibrary.Name;
                App.CurrentSetting.libServer = selectedLibrary.Server;
                MessageBox.Show("已选择 " + selectedLibrary.Name);
                NavigationService.GoBack();
            }
        }

        public class Group<T> : IEnumerable<T>
        {
            public Group(string name, IEnumerable<T> items)
            {
                this.Category = name;
                this.Items = new List<T>(items);
            }

            public override bool Equals(object obj)
            {
                Group<T> that = obj as Group<T>;

                return (that != null) && (this.Category.Equals(that.Category));
            }

            public string Category { get; set; }

            public IList<T> Items { get; set; }

            #region IEnumerable<T> Members

            public IEnumerator<T> GetEnumerator()
            {
                return this.Items.GetEnumerator();
            }

            #endregion

            #region IEnumerable Members

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return this.Items.GetEnumerator();
            }

            #endregion
        }
    }
}
