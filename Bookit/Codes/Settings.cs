﻿using System;
using System.IO.IsolatedStorage;
using System.ComponentModel;

namespace Bookit
{
    /// <summary>
    /// 应用程序设置类
    /// </summary>
    public class Settings : INotifyPropertyChanged
    {
        public string libKey, libServer;
        private string libName;
        public string LibName 
        {
            get { return libName; }
            set
            {
                libName = value;
                NotifyPropertyChanged("LibName");
            }
        }
        
        public string searchHistory;
        public bool isSaveHistory;
        public bool isFirstRun;		//是否为第一次运行
        public string keyWord;

        public static Settings Load()
        {
            if (App.storageSettings.Contains("Settings"))
            {
                return App.storageSettings["Settings"] as Settings;
            }
            else
            {
                return SetDefault();
            }
        }

        /// <summary>
        /// 恢复默认设置
        /// </summary>
        /// <returns>默认设置Settings类</returns>
        public static Settings SetDefault()
        {
            //TODO: 将变量置为默认
            Settings settings = new Settings
            {
                LibName = "西安电子科技大学",
                libKey = "xidian",
                libServer = "sinaapp",
                searchHistory = String.Empty,
                isSaveHistory = true,
                isFirstRun = true,
                keyWord = String.Empty
            };
            App.BorrowListInstance.ClearList();
            return settings;
        }

        public void Save()
        {
            App.storageSettings["Settings"] = this;
            App.storageSettings.Save();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
