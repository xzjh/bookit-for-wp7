﻿using System;
using System.Net;
using System.Collections.ObjectModel;
using System.Windows;
using Newtonsoft.Json.Linq;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using System.IO.IsolatedStorage;
using Bookit.Resources;

namespace Bookit
{
    /// <summary>
    /// SearchResultList类用来管理图书查询结果列表
    /// </summary>
    public class SearchResultList
    {
        public ObservableCollection<Book> TheList;   //图书列表
        public delegate void callbackDelegate(int isSuccess);
        //private PhoneApplicationPage PageInstance { get; set; }
        public class SearchResultArgs
        {
            public int MaxResults { get; set; }
            public int TotalResults { get; set; }
            public int StartIndex { get; set; }
            public int ListIndex { get; set; }
        }
        public SearchResultArgs SearchResultArgsIns { get; set; }

        public SearchResultList()
        {
            TheList = new ObservableCollection<Book>();
            SearchResultArgsIns = new SearchResultArgs()
            {
                MaxResults = 20,
                StartIndex = 1,
                TotalResults = 0,
                ListIndex = 0
            };
        }

        /// <summary>
        /// 从网络获取图书搜索结果列表
        /// </summary>
        public void LoadSearchResultListFromNet(string keywords, callbackDelegate Callback, int maxResults = 20, int startIndex = 1)
        {
            Uri doubanUri = new Uri("http://api.douban.com/book/subjects?q=" + HttpUtility.UrlEncode(keywords)
                + "&start-index=" + startIndex
                + "&max-results=" + maxResults
                + "&alt=json");
            try
            {
                WebClient client = new WebClient();
                client.DownloadStringCompleted += new DownloadStringCompletedEventHandler((sender, e) =>
                {                   //获取成功后的回调方法
                    try
                    {
                        //清空列表
                        TheList.Clear();
                        GetListFromJson(e.Result);
                        foreach (Book thisBook in TheList)
                        {
                            thisBook.getHoldingInfoFromNet();
                        }
                        Callback(isSuccess: TheList.Count); //搜索成功并返回结果数目
                    }
                    catch (Exception)
                    {
                        Callback(isSuccess: -1);
                    }
                });
                client.DownloadStringAsync(doubanUri);
            }
            catch (Exception)
            {
                Callback(isSuccess: -1);
            }
        }

        /// <summary>
        /// 从JSON中获取列表
        /// </summary>
        public void GetListFromJson(string jsonString)
        {
            JObject jObject = JObject.Parse(jsonString);
            //判断是否查询为空
            App.SearchResultListInstance.SearchResultArgsIns.TotalResults = jObject
                .Value<JObject>("opensearch:totalResults").Value<int>("$t");
            if (App.SearchResultListInstance.SearchResultArgsIns.TotalResults.Equals(0))
            {
                return;
            }
            JArray bookJSON = jObject.Value<JArray>("entry");
            int bookLength = bookJSON.Count;
            for (int i = 0; i < bookLength; i++)
            {
                Book thisBook = new Book();
                //获取图书标题
                thisBook.Title = bookJSON[i].Value<JObject>("title")
                    .Value<string>("$t");
                //获取图书封面图片uri
                thisBook.ImageUri = bookJSON[i]["link"][2].Value<string>("@href");
                if(thisBook.ImageUri.Contains("book-default-small"))            //无图片
                    thisBook.ImageUri = Bookit.Resources.AppStrings.EmptyImage;
                //获取书的属性
                JArray dbJSON = bookJSON[i].Value<JArray>("db:attribute");
                for (int j = 0; j < dbJSON.Count; j++)
                {
                    string key = dbJSON[j].Value<string>("@name");
                    string value = dbJSON[j].Value<string>("$t");
                    if (key.Equals("isbn13"))
                    {
                        //获取书的isbn号
                        thisBook.Isbn = value;

                    }
                    else if (key.Equals("publisher"))
                    {
                        //获取书的出版社信息
                        thisBook.Publisher = value;
                    }
                    else if (key.Equals("author"))
                    {
                        //获取图书作者信息
                        if (thisBook.Authors.Equals(string.Empty))
                        {
                            thisBook.Authors = value;
                        }
                        else    //多个作者
                        {
                            thisBook.Authors += "，" + value;
                        }
                    }
                    else if (key.Equals("translator"))
                    {
                        //获取图书译者信息
                        if (thisBook.Translators.Equals(string.Empty))
                        {
                            thisBook.Translators = value;
                        }
                        else //多个译者
                        {
                            thisBook.Translators += "，" + value;
                        }
                    }
                    else if (key.Equals("price"))
                    {
                        //获取图书价格信息
                        thisBook.Price = value;

                    }
                    else if (key.Equals("pubdate"))
                    {
                        //获取图书出版日期
                        thisBook.PubDate = value;
                    }
                }
                if (thisBook.Authors.Trim().Equals(string.Empty))
                    thisBook.Authors = AppStrings.AuthorNone;
                if (thisBook.Publisher.Equals(string.Empty))
                    thisBook.Publisher = AppStrings.PublisherNone;
                if (thisBook.Isbn.Equals(string.Empty))
                    thisBook.Isbn = AppStrings.ISBNNone;
                if (thisBook.Translators.Equals(string.Empty))
                    thisBook.Translators = AppStrings.TranslatorNone;
                if (thisBook.Price.Equals(string.Empty))
                    thisBook.Price = AppStrings.PriceNone;
                if (thisBook.PubDate.Equals(string.Empty))
                    thisBook.PubDate = AppStrings.PubDateNone;
                //获取图书评分
                thisBook.Rating = bookJSON[i]["gd:rating"]
                    .Value<string>("@average")
                    + "/10（"
                    + bookJSON[i]["gd:rating"].Value<string>("@numRaters")
                    + "人评价）";
                //thisBook.Index = i;
                TheList.Add(thisBook);
            }

        }

        public void SaveCache()
        {
            App.storageSettings["SearchResultList"] = this;
            App.storageSettings.Save();
        }

        public static SearchResultList LoadFromCache()
        {
            if (App.storageSettings.Contains("SearchResultList"))
                return App.storageSettings["SearchResultList"] as SearchResultList;
            else
                return new SearchResultList();
        }
    }
}
