﻿using System;
using System.Windows.Data;
using Bookit.Resources;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Windows;

namespace Bookit
{
    public class GeneralClass
    {
        public const string updateUri = "http://bookitapp.org/wp7/update.json";
        public enum AppUpdateMode { Normal, Silent };
        public static void CheckUpdate(AppUpdateMode mode)
        {
            try
            {
                WebClient client = new WebClient();
                client.DownloadStringCompleted += new DownloadStringCompletedEventHandler((sender, e) =>
                {
                    try
                    {
                        JObject updateJson = JObject.Parse(e.Result);
                        //判断版本号是否一致
                        float new_ver = updateJson.Value<float>("app_ver");
                        float cur_ver = Convert.ToSingle(AppStrings.AppVersion.Substring(0, AppStrings.AppVersion.IndexOf(' ')));
                        if (new_ver > cur_ver)                            //有新版本
                        {
                            string ver_name = updateJson.Value<string>("ver_name");
                            string app_feature = updateJson.Value<string>("new_feature");
                            MessageBox.Show("版本号：" + ver_name + "\n新特性：\n" + app_feature + "\n您可以到市场中更新或访问我们的项目主页下载", "新版本发布！", MessageBoxButton.OK);
                        }
                        else if (mode == AppUpdateMode.Normal)
                        {
                            MessageBox.Show("您目前使用的是最新版本");
                        }
                    }
                    catch
                    {
                        if (mode == AppUpdateMode.Normal)
                        {
                            MessageBox.Show("网络异常");
                        }
                    }
                });
                client.DownloadStringAsync(new Uri(updateUri + "?time=" + GetUnixTime()));
            }
            catch (Exception)
            {
                //do nothing
            }
        }

        public static string GetUnixTime()
        {
            return (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds.ToString();
        }
    }

    /// <summary>
    /// Availability颜色Converter
    /// </summary>
    public class AvailabilityColorConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value.ToString().Equals(AppStrings.HoldingInfoLoading))     //没有项
            {
                return "#875235";
            }
            else if (value.ToString().Equals(AppStrings.HoldingInfoFailed))
            {
                return "#708069";
            }
            else if (value.ToString().Equals(AppStrings.HoldingInfoNone))
            {
                return "#CF0C3D";
            }
            else
            {
                return "#4B9100";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Library类
    /// </summary>
    public class LibraryItem
    {
        public string Category { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
        public string Server { get; set; }
    }
}
