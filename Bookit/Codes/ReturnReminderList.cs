﻿using System.Collections.ObjectModel;
using Microsoft.Phone.Scheduler;

namespace Bookit
{
    public class ReturnReminderList
    {
        public ObservableCollection<ReturnReminder> TheList { get; set; }

        public ReturnReminderList()
        {
            TheList = new ObservableCollection<ReturnReminder>();
        }

        /// <summary>
        /// 从本地载入提醒列表
        /// </summary>
        public static ReturnReminderList LoadReturnReminderListFromLocal()
        {
            if (App.storageSettings.Contains("ReturnReminderList"))
                return App.storageSettings["ReturnReminderList"] as ReturnReminderList;
            else
            {
                ReturnReminderList newList = new ReturnReminderList();
                return newList;
            }
        }

        /// <summary>
        /// 清空列表
        /// </summary>
        public void ClearList()
        {
            TheList.Clear();
            SaveList();
        }

        public void SaveList()
        {
            App.storageSettings["ReturnReminderList"] = this;
            App.storageSettings.Save();
        }

        public void RemoveReminder(int index)
        {
            if (ScheduledActionService.Find(TheList[index].Name) != null)
                ScheduledActionService.Remove(TheList[index].Name);
            TheList.RemoveAt(index);
        }
    }
}
