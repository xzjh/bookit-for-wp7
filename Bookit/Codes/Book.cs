﻿using System;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Windows;
using Microsoft.Phone.Controls;
using System.ComponentModel;
using Bookit.Resources;

namespace Bookit
{
    /// <summary>
    /// Book类用来存储书籍信息以及提供相关信息处理过程
    /// </summary>
    public class Book : INotifyPropertyChanged
    {
        //豆瓣信息
        //书名
        private string title;
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                NotifyPropertyChanged("Title");
            }
        }
        //ISBN
        private string isbn;
        public string Isbn
        {
            get { return isbn; }
            set
            {
                isbn = value;
                NotifyPropertyChanged("Isbn");
            }
        }
        //作者
        private string authors;
        public string Authors
        {
            get { return authors; }
            set
            {
                authors = value;
                NotifyPropertyChanged("Authors");
            }
        }
        //简介
        private string summary;
        public string Summary
        {
            get { return summary; }
            set
            {
                summary = value;
                NotifyPropertyChanged("Summary");
            }
        }
        //图像地址
        private string imageUri;
        public string ImageUri
        {
            get { return imageUri; }
            set
            {
                imageUri = value;
                NotifyPropertyChanged("ImageUri");
            }
        }
        //评分
        private string rating;
        public string Rating
        {
            get { return rating; }
            set
            {
                rating = value;
                NotifyPropertyChanged("Rating");
            }
        }
        //出版社
        private string publisher;
        public string Publisher
        {
            get { return publisher; }
            set
            {
                publisher = value;
                NotifyPropertyChanged("Publisher");
            }
        }
        //译者
        private string translators;
        public string Translators
        {
            get { return translators; }
            set
            {
                translators = value;
                NotifyPropertyChanged("Translators");
            }
        }
        //出版日期
        private string pubDate;
        public string PubDate
        {
            get { return pubDate; }
            set
            {
                pubDate = value;
                NotifyPropertyChanged("PubDate");
            }
        }
        //官方定价
        private string price;
        public string Price
        {
            get { return price; }
            set
            {
                price = value;
                NotifyPropertyChanged("Price");
            }
        }
        //图书馆信息
        /*
         * holdingInfo和availability的几种状态
         * holdingInfo_loading
         * holdingInfo_failed
         * holdingInfo_available: 内容为馆藏信息,无对应R.string
         * holdingInfo_unavailable
         */
        //图书馆是否存在此书
        private string availability;
        public string Availability
        {
            get { return availability; }
            set
            {
                availability = value;
                NotifyPropertyChanged("Availability");
            }
        }
        //馆藏信息
        private string holdingInfo;
        public string HoldingInfo
        {
            get { return holdingInfo; }
            set
            {
                holdingInfo = value;
                NotifyPropertyChanged("HoldingInfo");
            }
        }
        //public int Index { get; set; }              //索引
        //若index==-1, 则代表当前项为emptyBook

        public Book()
        {
            Title = "";
            Isbn = "";
            Authors = "";
            Summary = "";
            ImageUri = "";
            Rating = "";
            Publisher = "";
            Translators = "";
            PubDate = "";
            Price = "";
            Availability = "";
            HoldingInfo = "";
            //Index = -1;
        }
        /// <summary>
        /// 从网络获取书籍简介
        /// summary的几种状态
        /// summary_none: 无简介信息
        /// summary_loading: 正在载入
        /// summary_failed: 载入失败
        /// 其他: 简介
        /// </summary>
        public void getSummaryFromNet()
        {
            this.Summary = AppStrings.SummaryLoading;
            //从豆瓣api获取summary后调用getSummaryFromJson
            Uri summaryUri = new Uri("http://api.douban.com/book/subject/isbn/" + HttpUtility.UrlEncode(this.Isbn) + "?alt=json");
            try
            {
                WebClient client = new WebClient();
                client.DownloadStringCompleted += new DownloadStringCompletedEventHandler((sender, e) =>
                {
                    try
                    {
                        getSummaryFromJson(e.Result);
                    }
                    catch (Exception)
                    {
                        this.Summary = AppStrings.SummaryFailed;
                    }
                });
                client.DownloadStringAsync(summaryUri);
            }
            catch (Exception)
            {
                this.Summary = AppStrings.SummaryFailed;
            }
        }

        /// <summary>
        /// 从JSON中获取书籍简介
        /// </summary>
        /// <param name="q">JSON字符串</param>
        private void getSummaryFromJson(string q)
        {
            try
            {
                JObject bookSummaryJson = JObject.Parse(q);
            }
            catch (Exception)
            {
                this.Summary = AppStrings.SummaryFailed;
                return;
            }
            try
            {
                JObject bookSummaryJSON = JObject.Parse(q);
                this.Summary = bookSummaryJSON["summary"].Value<string>("$t");
                if (this.Summary.Replace("\n", "").Trim().Equals(""))
                    this.Summary = AppStrings.SummaryNone;
            }
            catch (Exception)
            {
                this.Summary = AppStrings.SummaryNone;
            }
        }

        /// <summary>
        /// 从网络获取馆藏信息
        /// </summary>
        public void getHoldingInfoFromNet()
        {
            //显示正在载入
            this.HoldingInfo = AppStrings.HoldingInfoLoading;
            this.Availability = AppStrings.HoldingInfoLoading;
            //调用getHoldingInfoFromJson
            Uri holdingInfoUri;
            string timeStamp = GeneralClass.GetUnixTime();        //Unix时间戳,跳过缓存
            if (App.CurrentSetting.libServer.Equals("vps"))
            {
                holdingInfoUri = new Uri(AppStrings.BaseUrlVps + "?isbn=" + this.Isbn + "&school=" + App.CurrentSetting.libKey + "&time=" + timeStamp + "&source=wp7");
            }
            else
            {
                holdingInfoUri = new Uri(AppStrings.BaseUrlSinaApp + "?isbn=" + this.Isbn + "&school=" + App.CurrentSetting.libKey + "&time=" + timeStamp + "&source=wp7");
            }

            
            try
            {
                WebClient client = new WebClient();
                client.DownloadStringCompleted += new DownloadStringCompletedEventHandler((sender, e) =>
                {
                    try
                    {
                        // 从网络下载馆藏信息json成功后的回调方法
                        getHoldingInfoFromJson(e.Result);
                    }
                    catch (Exception)
                    {
                        this.HoldingInfo = AppStrings.HoldingInfoFailed;
                        this.Availability = AppStrings.HoldingInfoFailed;
                    }
                });
                client.DownloadStringAsync(holdingInfoUri);
            }
            catch (Exception)
            {
                this.HoldingInfo = AppStrings.HoldingInfoFailed;
                this.Availability = AppStrings.HoldingInfoFailed;
            }
        }

        private void getHoldingInfoFromJson(string q)
        {
            try
            {
                JObject holdingInfoJson = JObject.Parse(q);
                int num = holdingInfoJson.Value<int>("num");
                if (num >= 1)
                {
                    //解析馆藏数据

                    //获取馆藏数量信息
                    Availability = "总数"
                        + Convert.ToString(num) + " "
                        + "可借"
                        + holdingInfoJson.Value<string>("numAvailable");
                    //获取馆藏索书号
                    HoldingInfo = "索书号:" + holdingInfoJson.Value<string>("bookno") + "\n";
                    //获取馆藏地点和馆藏状态
                    JArray bookInfo = holdingInfoJson.Value<JArray>("info");
                    for (int i = 0; i < num; i++)
                    {
                        HoldingInfo += "\n"
                            + bookInfo[i].Value<string>("place").ToString()
                            + " "
                            + bookInfo[i].Value<string>("state").ToString();
                    }

                }
                else if (num == 0)
                {
                    //没有馆藏数据
                    HoldingInfo = AppStrings.HoldingInfoNone;
                    Availability = AppStrings.HoldingInfoNone;
                }
                else if (num == -1)
                {
                    //服务器无法连接到图书馆
                    HoldingInfo = AppStrings.HoldingInfoFailed;
                    Availability = AppStrings.HoldingInfoFailed;
                }
                else if (num == -2)
                {
                    //不支持该校
                    HoldingInfo = AppStrings.HoldingInfoFailed;
                    Availability = AppStrings.HoldingInfoFailed;
                }

                int numAvailable = holdingInfoJson.Value<int>("numAvailable");
            }
            catch (Exception)
            {
                //网络连接错误
                HoldingInfo = AppStrings.HoldingInfoFailed;
                Availability = AppStrings.HoldingInfoFailed;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
