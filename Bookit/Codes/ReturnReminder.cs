﻿using System;
using System.ComponentModel;

namespace Bookit
{
    public class ReturnReminder : INotifyPropertyChanged
    {
        //reminder名称
        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }
        //提醒内容
        private string content;
        public string Content
        {
            get { return content; }
            set
            {
                content = value;
                NotifyPropertyChanged("Content");
            }
        }
        //开始时间
        private DateTime startTime;
        public DateTime StartTime
        {
            get { return startTime; }
            set
            {
                startTime = value;
                NotifyPropertyChanged("StartTime");
            }
        }

        public ReturnReminder()     //一般不调用这个
        {
            Content = String.Empty;
            StartTime = DateTime.Now.AddMinutes(-1);        //默认不提醒
            Name = String.Empty;
        }

        public ReturnReminder(string reminderName, string reminderContent, DateTime reminderStartTime)
        {
            Name = reminderName;
            Content = reminderContent;
            StartTime = reminderStartTime;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
