﻿using System;
using System.Net;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.IO;
using System.Windows;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Bookit
{
    /// <summary>
    /// BorrowList类用来管理图书列表
    /// </summary>
    public class BorrowList
    {
        public ObservableCollection<Book> TheList { get; set; }   //图书列表
        //private const string emptyImage = @"Resources/emptyimage.jpg";
        public int currentIndex = 0;
        public BorrowList()
        {
            TheList = new ObservableCollection<Book>();
        }

        /// <summary>
        /// 从本地载入借阅列表
        /// </summary>
        public static BorrowList LoadBorrowListFromLocal()
        {
            if (App.storageSettings.Contains("BorrowList"))
                return App.storageSettings["BorrowList"] as BorrowList;
            else
            {
                BorrowList newList = new BorrowList();
                newList.Add(NoItem());
                return newList;
            }
        }

        private static Book NoItem()
        {
            Book noItem = new Book();
            noItem.Title = "无借阅图书";
            noItem.ImageUri = Bookit.Resources.AppStrings.EmptyImage;
            //noItem.Index = -1;
            return noItem;
        }

        /// <summary>
        /// 清空列表
        /// </summary>
        public void ClearList()
        {
            TheList.Clear();
            SaveList();
        }
        public void SaveList()
        {
            if (TheList.Count == 0)
                TheList.Add(NoItem());
            App.storageSettings["BorrowList"] = this;
            App.storageSettings.Save();
        }
        public void CheckIfEmpty()
        {
            if (TheList.Count == 0)
            {
                TheList.Add(NoItem());
            }
        }
        public void Add(Book thisBook)
        {
            //如果没有书，则清空列表
            if (TheList.Count == 1 && TheList[0].ImageUri == Bookit.Resources.AppStrings.EmptyImage)
            {
                TheList.Clear();
            }
            //thisBook.Index = TheList.Count;
            TheList.Add(thisBook);
        }
    }
}
