### 1.2 Beta [2012.08.08] ###

* 新增图书归还提醒功能
* 修正版本号错误

### 1.1 Beta [2012.02.06] ###

* 修复搜索结果为零提示网络错误的bug
* 自动升级网络异常时的提示
* 安装程序得到精简,体积大大减小
* 修改关于简介内容

### 1.0 Beta [2011.12.31] ###

* 基本功能
	* 图书搜索
	* 图书信息查询
	* 图书馆藏查询
	* 全国100多所学校的支持
	* 条码扫描功能
* 增加借阅列表功能
* 增加图书详细信息页前后书目翻页的功能
* 增加搜索结果页面翻页的功能
* 增加自动检查更新功能
